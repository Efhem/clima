//
//  WeatherManager.swift
//  Clima
//
//  Created by Femi Adegbite on 10/04/2020.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation

protocol WeatherManagerDelegate {
    
    func didUpdateWeather(_ weatherManager: WeatherManager, weather: WeatherModel)
    func didFailwithError(error: Error)
}

struct WeatherManager {
    let url = "https://api.openweathermap.org/data/2.5/weather?appid=e6d7a6a2cf08aba104f1ab1a5ddcc79d&units=metric"
    
    func fetchWeather(cityName: String){
        let urlString = "\(url)&q=\(cityName)"
        performRequest(with: urlString)
    }
    
    func fetchWeather(latitude: Double, longitudde: Double){
        let urlString = "\(url)&lat=\(latitude)&lon=\(longitudde)"
        performRequest(with: urlString)
    }
    
    var delegate: WeatherManagerDelegate?
    
    
    func performRequest(with urlString: String) {
        
        if let url = URL(string: urlString){
            print(url)
            
            let session = URLSession(configuration: .default)
            
            let task = session.dataTask(with: url) { (data, response, error) in
                
                if(error !=  nil){
                    self.delegate?.didFailwithError(error: error!)
                    return
                }
                if let safeData = data {
                    if let weather = self.parseJSON(safeData){
                        self.delegate?.didUpdateWeather(self, weather: weather)
                    }
                }
            }
            
            //print("current url: \(String(describing: task.currentRequest))")
            //print("original url: \(String(describing: task.originalRequest))")
            task.resume()
        }
    }
    
    fileprivate func parseJSON(_ weatherData: Data) -> WeatherModel? {
        let decoder = JSONDecoder()
        
        do{
            let decodedData = try decoder.decode(WeatherData.self, from: weatherData)
            
            let id = decodedData.weather[0].id
            let cityName = decodedData.name
            let temperature = decodedData.main.temp
            
            return WeatherModel(conditionId: id, cityName: cityName, temperature: temperature)
        }catch {
            delegate?.didFailwithError(error: error)
            return nil
        }
        
    }
}
